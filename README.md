# ftpbro
An easy to use FTP accounts helper.

## Roadmap
- [ ] Create multiple FTP accounts in one command
- [ ] Use JSON files
- [ ] Use CSV files
- [ ] Auto generate password w/ default complexity but customisable

## Compatibility
- :penguin: Linux : **required**

## Notes
- I use this project to learn some Rust :wink: 
- I might try to add it in some package managers or make it easy to install/update ?
